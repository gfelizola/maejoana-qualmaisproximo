﻿var BROWSER = '';
var V_EXT = '';
var A_EXT = '';

var iniciaAnima = function(){
		
	$('bgMobPlay').unbind('click');	
	
	if(Main.tituloWithAudio){
		$('#mc_logo_intro').fadeOut(500);
		if(Main.hasPreRun) 
			Main.preRun();
		playAudio('audio','titulo',function(){
			$('#mc_titulo_intro').fadeOut(500);
			Main.iniciar();	
		});
	}else{
		$('#mc_logo_intro, #mc_titulo_intro').fadeOut(500);
		Main.iniciar();	
	}	
	$('#bgMobPlay').fadeOut('',function(){ 
		$('#bgMobPlay')
		.removeClass('play')
		.addClass('pause'); 
	});	
	if(Main.isTablet){
		$('#stage').addClass('tablet');
	}
}

var Main = {
	titulo: '',
	tituloWithAudio: false,
	hasPreRun: false,
	_stage_w: 800,
	_stage_h: 600,
	
	isTablet: false,
	preRun: function(){},
	init: function(){	
		this.mobileInit();
		PlayerController.init();
		
		$('#bgMobPlay').bind('click',iniciaAnima);
		
		$('.titulo').html(Main.titulo);
		$('title, .titulo.noBR').html(Functions.strip_tags(Main.titulo));
		
		var countLinhas = Main.titulo.match(/\<br\/\>/gi);  
		if(countLinhas && countLinhas.length>0){
			$('#mc_titulo_intro, .abertura3 h2').addClass('l'+(countLinhas.length+1));
		}
	},
	
	initPreloader: function(){		
		var loaderAnimation = $("#html5Loader").LoaderAnimation({ lineWidth:10, color:"#ffc905", glowColor:"#fff", radius:50, font:"normal bold 14px Arial" });
		$.html5Loader({
			getFilesToLoadJSON:filesJSON,
			onUpdate: loaderAnimation.update
		});
	},
	mobileInit:function()
	{
		var that = this;		
		
		uaBrowser	= function(a){
			var 
				a= a.toLowerCase(),
				c=/(opera)(?:.*version)?[ \/]([\w.]+)/,
				b=/(msie) ([\w.]+)/,
				e=/(mozilla)(?:.*? rv:([\w.]+))?/,
				a=/(webkit)[ \/]([\w.]+)/.exec(a)||c.exec(a)||b.exec(a)||0>a.indexOf("compatible")&&e.exec(a)||[];
			
			return{
				browser:a[1]||"",
				version:a[2]||"0"
			}
		};
		
		uaPlatform = function(a){
			var b=a.toLowerCase(),
				d=/(android)/,
				e=/(mobile)/,
				a=/(ipad|iphone|ipod|android|blackberry|playbook|windows ce|webos)/.exec(b)||[],
				b=/(ipad|playbook)/.exec(b)||!e.exec(b)&&d.exec(b)||[];
				
			a[1]&&(a[1]=a[1].replace(/\s/g,"_"));
			
			return{
				platform:a[1]||"",
				tablet:b[1]||""
			}
		};
		
		browser={};
		platform={};
		var i=uaBrowser(navigator.userAgent);
		
		browser && ( browser[i.browser] =! 0, browser.version = i.version);
		i = uaPlatform(navigator.userAgent);
		platform && ( platform[i.platform] =! 0, platform.mobile =! i.tablet, platform.tablet =!! i.tablet);
		
		if(platform.tablet) {
			that.isTablet = true;
		} 

	},	
	iniciar: function(){
		InterfaceController.init();
	}
}

var	volume = 1;
function setVolume(){
	var objects = document.getElementsByClassName('volumeController');
	var i = 0,
		t = objects.length;
	for(i = 0; i < t; i++){
		if(objects[i]) objects[i].volume = volume;
	}
	if(volume == 0){
		$('#btSom').attr('data-status','off');
	}else{
		$('#btSom').attr('data-status','on');
	}
}
function stopAllSongs(){
	var objects = document.getElementsByClassName('volumeController');
	var i = 0,
		t = objects.length;
	for(i = 0; i < t; i++){
		if(objects[i]){
			objects[i].pause();
		}
	}
}

function mute(){
	var objects = document.getElementsByClassName('volumeController');
	var i = 0,
		t = objects.length;
	for(i = 0; i < t; i++){
		if(objects[i]) objects[i].muted=true;
	}
}

function unmute(){
	var objects = document.getElementsByClassName('volumeController');
	var i = 0,
		t = objects.length;
	for(i = 0; i < t; i++){
		if(objects[i]) objects[i].muted=false;
	}
}



function playAudio(tag,nome,callback){	
	var audio = document.getElementById(tag);
	audio.src = "application/assets/audios/"+nome+A_EXT;
	$(audio).unbind('ended');
	if(callback){
		$(audio).bind('ended',callback);
	}
	audio.play();
}


function playPausedAudio(tag){
	var audio = document.getElementById(tag);
	audio.play();
}
function pauseAudio(tag){
	var audio = document.getElementById(tag);
	audio.pause();
}

$(document).ready(function()
	{ 
		Functions.verificaBrowser();
		Main.init();
		Functions.updateScale();
		$(window).resize(function(){ Functions.updateScale(); });
	}
);