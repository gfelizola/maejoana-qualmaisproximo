Main.preRun         = function(){}

var debug           = false; //variavel usada para desenvolvimento

var nivelAtual      = 1;
var vermelhaVisivel = false;
var verdeVisivel    = false;
var jogadasNivel1   = 0;
var jogadasNivel2   = 0;
var rodadasNivel1   = 0;
var rodadasNivel2   = 0;
var pontosNivel1    = 0;
var pontosNivel2    = 0;
var certo1          = false;
var certo2          = false;
var valendo         = true;
var mostrarConfirm  = true;
var maximoJogadas   = debug ? 2 : 5;
var maximoRodadas   = debug ? 2 : 5;
var balancando      = false;
var TEMPO_INICIO    = debug ? 5 : 20;
var timer           = { t: TEMPO_INICIO };
var tocouNivel      = false;
var mostrouCredito  = false;


var audios = {
	acerto           : ["2.1a", "2.1b", "2.1c"],
	erro             : ["2.2a", "2.2b", "2.2c"],
	tempo            : "2.3",
	escolha          : "2.4",
	nivelCompleto    : "2.5",
	jogoCompleto     : "2.6",
	nivelCompletoR   : "2.7",
	confirmarEncerra : "2.8",
	relogio          : "Relogio"

}

function tocarAudio (audioItem, cb) {
	if( $.isArray(audioItem) ){
		audioItem = audioItem[ getRandomInt(0, audioItem.length - 1)];
	}

	var nomeAudio = "cena" + audioItem;
	playAudio('audio', nomeAudio, cb);
}

function tocarTrilha() {
	var rodadaTrilha = ( nivelAtual == 1 ? rodadasNivel1 : rodadasNivel2 ) + 1
	var nomeAudio = "trilha" + rodadaTrilha;
	playAudio('trilha', nomeAudio, tocarTrilha);

	mostrarCredito("trilha" + rodadaTrilha);
}

function mostrarCredito (qual) {
	$("#jogo .credito small").hide();
	$("#jogo .credito ." + qual).show();
	$("#jogo .credito").fadeIn().delay(5000).fadeOut();;
}

function inicioAnimacao()
{
	configJogo();
	$('#interatividade').fadeIn(500);

	if( debug ){
		mostrarInicio();
	} else {
		mostrarInstrucoes();
	}
}

function reset() {
	nivelAtual      = 1;
	vermelhaVisivel = false;
	verdeVisivel    = false;
	jogadasNivel1   = 0;
	jogadasNivel2   = 0;
	rodadasNivel1   = 0;
	rodadasNivel2   = 0;
	pontosNivel1    = 0;
	pontosNivel2    = 0;
	certo1          = false;
	certo2          = false;
	valendo         = false;
	mostrarConfirm  = true;
	tocouNivel      = false;
	mostrouCredito  = false;
}

function configJogo(){
	reset();

	$("#exemploDeVideo .fechar").unbind('click').click(function(event) {
		$("#exemploDeVideo").fadeOut(500);
		reiniciarJogo();
	});

	$('#interface #btnMenu').unbind('click').bind('click',reinciar);
	$('#interface #btnReiniciar').unbind('click').bind('click',reinciar);
	$('#interface #btnAjuda').unbind('click').bind('click',mostrarInstrucoes);

	$("#jogo .btn-jogar").unbind('click').bind('click',comecarJogo);
	$("#jogo .btn-mostrar").unbind('click').bind('click',mostrarCartas);
	$("#escolha .btn-nivel1").unbind('click').bind('click',jogarNivel1);
	$("#escolha .btn-nivel2").unbind('click').bind('click',jogarNivel2);
	$("#escolha .btn-encerrar").unbind('click').bind('click',encerrarJogo);
	$("#escolha .placa-continuar .btn-sim").unbind('click').bind('click',confirmaEncerrar);
	$("#escolha .placa-continuar .btn-nao").unbind('click').bind('click',cancelaEncerrar);

	$("#jogo .carta-verde").unbind('click').bind('click', verificaCartaCerta);

	$("#pontuacao .btn-reiniciar").unbind('click').bind('click', reiniciarJogo);
}

function reiniciarJogo(){
	configJogo();

	InterfaceController.canPause = false;
	InterfaceController.canPlay = false;
	PlayerController.evEndedVideoFunction = function(e){}
	PlayerController.evPlayingVideoFunction = function(e){}
	PlayerController.changeVideo('');

	$("#exemploDeVideo").fadeOut(300);
	$("#escolha").fadeOut(300);
	$("#pontuacao").fadeOut(300);
	$("#jogo").fadeIn(300);

	$("#jogo .carta-entrada.nivel1").fadeIn(300);
	$("#jogo .carta-wrapper").fadeOut(300);
	$("#jogo .btn-mostrar").fadeOut(300);
	$("#jogo .placar").fadeOut(300);

	fecharTodasCartas();
	atualizarPlacar();
	mostrarInicio();

	PlayerController.playPause();
}

function abrirCarta(carta){
	$(carta).find(".tras").fadeOut('300');
	$(carta).find(".frente").fadeIn('300');
}

function fecharCarta(carta){
	$(carta).find(".tras").fadeIn('300');
	$(carta).find(".frente").fadeOut('300');
	$(carta).removeClass('ativa');
}

function fecharTodasCartas(){
	if( nivelAtual == 1 && jogadasNivel1 == maximoJogadas ){
		rodadasNivel1++;
		mostrarOpcoesDeNivel();
	} else if( nivelAtual == 2 && jogadasNivel2 == maximoJogadas ){
		rodadasNivel2++;
		mostrarOpcoesDeNivel();
	} else {
		fecharCarta(".carta-wrapper");
		valendo         = false;
		vermelhaVisivel = false;
		verdeVisivel    = false;
		TweenMax.delayedCall( 0.2, calculaNumeros );	
	}
}

function verificaCartaCerta(){
	if ( valendo ) {
		valendo = false;
		if( nivelAtual == 1 ){
			jogadasNivel1++ ;
		} else {
			jogadasNivel2++ ;
		}

		$(this).addClass('ativa');
		pararTimer();
		
		if ( $(this).hasClass('carta1') && certo1 ) {
			mostraAcerto( false );
			nivelAtual == 1 ? pontosNivel1 += 2 : pontosNivel2 += 2 ;
			atualizarPlacar();
		} else if ( $(this).hasClass('carta2') && certo2 ) {
			mostraAcerto( true );
			nivelAtual == 1 ? pontosNivel1 += 2 : pontosNivel2 += 2 ;
			atualizarPlacar();
		} else {
			mostraErro( $(this).hasClass('carta1') );
			nivelAtual == 1 ? pontosNivel1 -= 1 : pontosNivel2 -= 1 ;
			atualizarPlacar();
		}	
	};
	
}

function mostraAcerto (outroLado) {
	tocarAudio( audios.acerto );
	var lIni = { left : -240, right : "auto" }
	var lFim = { left : -20 }

	var tl = new TimelineMax({onComplete: fecharTodasCartas});

	if( outroLado ){
		$(".mao-certo").addClass('outro-lado');
		lIni = { right : -240, left : "auto" }
		lFim = { right : -20 }
	} else {
		$(".mao-certo").removeClass('outro-lado');
	}

	tl.set( $(".mao-certo").show(), { css: lIni } );
	tl.to( $(".mao-certo"), 0.4, { css: lFim } );
	tl.to( $(".mao-certo"), 0.3, { top: 370, ease:Power2.easeOut } );
	tl.to( $(".mao-certo"), 0.3, { top: 380, ease:Power2.easeOut } );
	tl.to( $(".mao-certo"), 0.3, { top: 370, ease:Power2.easeOut } );
	tl.to( $(".mao-certo"), 0.3, { top: 380, ease:Power2.easeOut } );
	tl.to( $(".mao-certo"), 0.4, { css: lIni } );
}

function mostraErro ( outroLado ) {
	tocarAudio( audios.erro );

	var lIni = { right : -240, left : "auto" }
	var lFim = { right : -20 }

	var tl = new TimelineMax({onComplete: fecharTodasCartas});

	if( outroLado ){
		$(".mao-errado").removeClass('outro-lado');
		lIni = { left : -240, right : "auto" }
		lFim = { left : -20 }
	} else {
		$(".mao-errado").addClass('outro-lado');
	}

	if( certo1 ){
		$(".mao-errado").addClass('outro-lado');
	} else {
		$(".mao-errado").removeClass('outro-lado');
	}

	var tl = new TimelineMax({onComplete: fecharTodasCartas});
	tl.set( $(".mao-errado").show(), { css: lIni } );
	tl.to( $(".mao-errado"), 0.4, { css: lFim } );
	tl.to( $(".mao-errado"), 0.4, { top: 410, ease:Power2.easeOut } );
	tl.to( $(".mao-errado"), 0.4, { top: 400, ease:Power2.easeOut } );
	tl.to( $(".mao-errado"), 0.4, { top: 410, ease:Power2.easeOut } );
	tl.to( $(".mao-errado"), 0.4, { top: 400, ease:Power2.easeOut } );
	tl.to( $(".mao-errado"), 0.4, { css: lIni } );
}

function mostrarInicio(){
	$("#interatividade").removeClass('nivel1 nivel2 pontuacao').addClass('nivel' + nivelAtual);
	$("#jogo").fadeIn(500);
	mostrarCredito("entrada");
	TweenMax.fromTo( $("#jogo .btn-jogar").show(), 0.5, { css:{ bottom:-100 } }, { css:{ bottom:70 }, ease:Back.easeOut } );
}

function mostrarCartas(){
	console.log("mostrouCredito", mostrouCredito);
	if( ! mostrouCredito ){
		mostrarCredito("efeitos");
		mostrouCredito = true;
	}

	if( ! valendo ){
		if( verdeVisivel ){
			abrirCarta( $(".carta-wrapper.carta-vermelha") );
			vermelhaVisivel = true ;

			pulsarBotaoParar();
			iniciarTimer();

			valendo = true;
		} else {
			abrirCarta( $(".carta-wrapper.carta-verde") );
			pulsarBotao();
			verdeVisivel = true;
		}	
	}
}

function pulsarBotao () {
	TweenMax.to( $("#jogo .btn-mostrar"), 0.7, { scale:1.1, opacity:0.9, yoyo:true, repeat:-1, ease:Strong.easeInOut } );
}

function pulsarBotaoParar() {
	TweenMax.killTweensOf( $("#jogo .btn-mostrar") );
	TweenMax.set( $("#jogo .btn-mostrar"), {scale:1, opacity:1} );
}

function calculaNumeros () {
	var max = nivelAtual == 1 ? 60 : 100 ;
	var min = nivelAtual == 1 ? 00 : 040 ;

	var nVermelho = getRandomInt( max, min );
	var nVerde1 = getRandomInt( max, min );

	while( nVerde1 == nVermelho ){
		nVerde1 = getRandomInt( max, min );
	}

	var diff = Math.abs( nVerde1 - nVermelho );
	var nVerde2 = getRandomInt( max, min );
	while( nVerde2 == diff || nVerde2 == nVerde1 || nVerde2 == nVermelho ){
		nVerde2 = getRandomInt( max, min );
	}

	var diff2 = Math.abs( nVerde2 - nVermelho );
	certo1 = diff < diff2;
	certo2 = diff2 < diff;

	// console.log( nVermelho, nVerde1, nVerde2 );

	$(".carta-vermelha .frente").text(nVermelho);
	$(".carta-verde.carta1 .frente").text(nVerde1);
	$(".carta-verde.carta2 .frente").text(nVerde2);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function mostraCartaCertaFimTempo(){
	pararTimer();

	if( certo1 ){
		$(".carta-verde.carta1").addClass('ativa');
	} else {
		$(".carta-verde.carta2").addClass('ativa');
	}

	mostraErro();
	nivelAtual == 1 ? pontosNivel1 -= 1 : pontosNivel2 -= 1 ;
	atualizarPlacar();

	$("#jogo .placar .relogio").addClass('alerta');
	TweenMax.to( $("#jogo .placar .relogio"), 0.02, { rotation:10, yoyo: true, repeat:100 } );

	tocarAudio( audios.tempo, function(){
		$("#jogo .placar .relogio").removeClass('alerta');
	});
}

function iniciarTimer () {
	timer.t = TEMPO_INICIO;
	balancando = false;
	$("#jogo .placar .relogio").removeClass('alerta');

	TweenMax.to( timer, TEMPO_INICIO, { t:0, onComplete:mostraCartaCertaFimTempo, ease:Linear.easeNone, onUpdate: function(){
		var tempo = Math.round( timer.t );
		$("#jogo .placar .relogio").text( tempo );
		if( tempo <= 4 && ! balancando ){
			// mostrarAlertaTempo();
			balancando = true ;
			tocarAudio( audios.relogio );
		}
	}});
}

function pararTimer () {
	TweenMax.killTweensOf( timer );
	TweenMax.killTweensOf( $("#jogo .placar .relogio") );
	TweenMax.set( $("#jogo .placar .relogio"), { rotation:0 } );
	$("#jogo .placar .relogio").addClass('alerta');
	
	balancando = false;
}

function mostrarAlertaTempo () {
	balancando = true;	
	TweenMax.to( $("#jogo .placar .relogio"), 0.2, { rotation:10, yoyo: true, repeat:-1, ease:Back.easeOut } );
}

function atualizarPlacar(){
	var pontos = pontosNivel1 + pontosNivel2 ;
	$("#jogo .placar .pontos").text(pontos + " PONTOS");
}

function comecarJogo(){
	tocarTrilha();
	calculaNumeros();
	fecharTodasCartas();

	$("#jogo .credito").stop().fadeOut(200);
	$("#jogo .placar .relogio").text(TEMPO_INICIO);

	$(".carta-entrada").fadeOut(300);
	$(".carta-wrapper").delay(300).fadeIn(300);
	$(".placar").delay(700).fadeIn(300);
	TweenMax.to( $("#jogo .btn-jogar").show(), 0.5, { css:{ bottom:-100 }, ease:Back.easeIn } );
	TweenMax.fromTo( $("#jogo .btn-mostrar").show(), 0.5, { css:{ bottom:-150 } }, { css:{ bottom:40 }, ease:Back.easeOut, delay: 0.4 } );
}

function jogarNivel1(){
	jogarNivel(1);
}

function jogarNivel2(){
	jogarNivel(2);
}

function jogarNivel( nivel ){
	jogadasNivel1 = 0;
	jogadasNivel2 = 0;
	fecharTodasCartas();
	atualizarPlacar();
	stopAllSongs();
	tocarTrilha();

	$("#interatividade").removeClass('nivel1 nivel2 pontuacao').addClass('nivel' + nivel);

	$("#escolha").fadeOut(500);
	$("#jogo").fadeIn(500);

	if( nivelAtual != nivel ){
		nivelAtual = nivel;

		$(".carta-wrapper").hide();
		$(".placar").hide();
		$(".carta-entrada").show(300);
		$("#jogo .btn-mostrar").hide();

		mostrarInicio();
	}
}

function encerrarJogo(){
	
	if( mostrarConfirm ){
		mostrarConfirmacaoEncerramento();
	} else {
		confirmaEncerrar();
	}
}

function mostrarConfirmacaoEncerramento(){
	$("#escolha .opcao").fadeOut(500);
	$("#escolha .placa-continuar").fadeIn(500, function () {
		tocarAudio( audios.confirmarEncerra );
	});
}

function confirmaEncerrar(){
	stopAllSongs();
	$("#interatividade").removeClass('nivel1 nivel2 pontuacao').addClass('pontuacao');
	$("#pontuacao .estrelas .estrela span").removeClass('cheia meia vazia');

	calculaPontuacao();

	$("#escolha").fadeOut(500);
	$("#pontuacao").fadeIn(500, animarEstrelas);
}

function calculaPontuacao () {
	var total = (pontosNivel1 + pontosNivel2);

	$("#pontuacao .estrelas .estrela span").removeClass('cheia meia vazia');

	if( total > 334 ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('cheia');
	} else if( total > 267 ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('meia');
	} else if( total > 200 ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('vazia');
	}  else if( total > 134 ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('meia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('vazia');
	} else if( total > 67 ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('cheia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('vazia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('vazia');
	} else if( total < 67 ){
		$("#pontuacao .estrelas .estrela-1 span").addClass('meia');
		$("#pontuacao .estrelas .estrela-2 span").addClass('vazia');
		$("#pontuacao .estrelas .estrela-3 span").addClass('vazia');
	}

	$("#pontuacao .placa .n1 .direita span").text( pontosNivel1 );
	$("#pontuacao .placa .n2 .direita span").text( pontosNivel2 );
	$("#pontuacao .placa .total .direita span").text( total );
}

function animarEstrelas () {
	// $("#pontuacao .estrelas .estrela span").removeClass('cheia meia vazia').addClass('cheia');
	TweenMax.allFrom( $("#pontuacao .estrelas .estrela span"), 0.4, { width:0, ease:Power2.easeOut }, 0.3 );
	TweenMax.allFrom( $("#pontuacao .estrelas .estrela span"), 0.4, { css:{ scale:0.1, opacity:0 }, ease:Back.easeOut }, 0.3 );
}

function cancelaEncerrar(){
	$("#escolha .opcao").fadeIn(500);
	$("#escolha .placa-continuar").fadeOut(500);	
}

function mostrarOpcoesDeNivel () {
	$("#jogo").fadeOut(500);
	$("#escolha").fadeIn(500);

	$("#escolha .opcao").show();
	$("#escolha .placa-continuar").hide();

	$("#escolha .opcao.nivel1 .numero").text( rodadasNivel1 + "/" + maximoRodadas );
	$("#escolha .opcao.nivel2 .numero").text( rodadasNivel2 + "/" + maximoRodadas );

	if( rodadasNivel1 >= maximoRodadas ){
		$("#escolha .opcao.nivel1").addClass('disable').find(".btn").unbind('click');
	} else {
		$("#escolha .opcao.nivel1").removeClass('disable').find(".btn").unbind('click').bind('click', jogarNivel1);
	}

	if( rodadasNivel2 >= maximoRodadas ){
		$("#escolha .opcao.nivel2").addClass('disable').find(".btn").unbind('click');
	} else {
		$("#escolha .opcao.nivel2").removeClass('disable').find(".btn").unbind('click').bind('click', jogarNivel2);
	}

	if( rodadasNivel1 >= maximoRodadas && rodadasNivel2 >= maximoRodadas ){
		mostrarConfirm = false;
		tocarAudio( audios.jogoCompleto );
	} else if( rodadasNivel1 >= maximoRodadas && nivelAtual == 1 ){
		if( ! tocouNivel ) {
			tocarAudio( audios.nivelCompleto );
			tocouNivel = true;
		} else {
			tocarAudio( audios.nivelCompletoR );
		}
	} else if( rodadasNivel2 >= maximoRodadas && nivelAtual == 2 ){
		if( ! tocouNivel ) {
			tocarAudio( audios.nivelCompleto );
			tocouNivel = true;
		} else {
			tocarAudio( audios.nivelCompletoR );
		}
	} else {
		tocarAudio( audios.escolha );
	}

	if( nivelAtual == 1 ){
		$("#escolha .opcao.nivel1 .btn").addClass('duas-linhas').html("CONTINUAR JOGANDO<BR>NO NÍVEL 1");
		$("#escolha .opcao.nivel2 .btn").removeClass('duas-linhas').html("IR PARA O NÍVEL 2");
	} else {
		$("#escolha .opcao.nivel1 .btn").removeClass('duas-linhas').html("IR PARA O NÍVEL 1");
		$("#escolha .opcao.nivel2 .btn").addClass('duas-linhas').html("CONTINUAR JOGANDO<BR>NO NÍVEL 2");
	}
}

function esconderSections() {
	$("#jogo").fadeOut(300);
	$("#escolha").fadeOut(300);
	$("#pontuacao").fadeOut(300);
	$("#exemploDeVideo").fadeOut(300);
}

function mostrarInstrucoes(){
	stopAllSongs();
	InterfaceController.closeAbas();
	esconderSections();
	$('#exemploDeVideo').stop().fadeIn(500);
	
	// ESSE É O CÓDIGO PARA INSERIR O PLAYER NO VÍDEO E NÃO É OBRIGATÓRIO
	$('#video').mediaelementplayer({
		features           : ['playpause','progress','duration'],
		clickToPlayPause   : false,
		autoRewind         : false,
		videoWidth         : 800,
		videoHeight        : 600,
		defaultVideoWidth  : 800,
		defaultVideoHeight : 600,
		success: function(player, node) {
			$('.mejs-overlay-play, .mejs-overlay-loading').remove();
			$('#video, .mejs-container').css('display','none');
			$('#exemploDeVideo .mejs-time-rail').width(739);
			$('#video').attr('class','abs volumeController');
		}
	});
	
	PlayerController.evEndedVideoFunction = reiniciarJogo;
	PlayerController.evPlayingVideoFunction = function(e){ 
		$('#video, .mejs-container').fadeIn(500);  
		InterfaceController.canPause = true;
		InterfaceController.canPlay = true;
	}
	PlayerController.changeVideo('application/assets/videos/video'+V_EXT);// V_EXT GERA A EXTENÇÃO CORRETA PARA O BROWSER EXECUTOR
	$('#exemploDeVideo .fechar').unbind('click').bind('click', reiniciarJogo);
}

function reinciar()
{
	stopAllSongs();
	InterfaceController.closeAbas();
	
	reiniciarJogo();
}

function backHome()
{
	stopAllSongs();
	InterfaceController.closeAbas();
	InterfaceController.canPause = false;
	InterfaceController.canPlay = false;
	PlayerController.evEndedVideoFunction = function(e){};
	PlayerController.evPlayingVideoFunction = function(e){};
}