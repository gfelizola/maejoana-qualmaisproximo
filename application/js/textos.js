﻿Textos.creditos = '<p><b>Coordenação geral:</b>  Kelly Mayumi Ishida</p>' +
	'<p><b>Coordenação de conteúdo:</b>  Maria José Guimarães de Souza</p>' +
	'<p><b>Roteiro:</b>  Luiz Marcio Pereira Imenes, Marcelo Cestari Terra Lellis</p>' +
	'<p><b>Edição:</b>  Felipe Jordani</p>' +
	'<p><b>Revisão técnica:</b>  Luiz Marcio Pereira Imenes, Marcelo Cestari Terra Lellis</p>' +
	'<p><b>Revisão de texto:</b>  Ramiro Morais Torres</p>' +
	'<p><b>Coordenação de arte:</b>  Eduardo Reche Bertolini</p>' +
	'<p><b>Edição de arte:</b>  Rodrigo Luis de Andrade</p>' +
	'<p><b>Iconografia:</b>  Renate Hartfiel</p>' +
	'<p><b>Coordenação de produção:</b>  Roberto Andrade Ono</p>' +
	'<p><b>Assistêncie de programação:</b>  Daniel Palmeira dos Passos, Renato Frias Rocha Ibiapina</p>' +
	'<p><b>Assistência de produção e checagem:</b>  Ana Maria Totaro Delgado, Cecília Japiassu Reis, Natalia Lamucio Andrade, Renata Campos Michelin, Valdeí Prazeres</p>' +
	'<p><b>Assistência editorial:</b>  Bruna Bressan Bosnic</p>' +
	'<p><b>Locução:</b> Amanda Cristina Baldi Nogueira, Letícia Bortoletto de Camargo</p>' +
	'<p><b>Produção:</b> Mãe Joana House</p>' +
	'<p>A editora realizou todos os esforços para localizar os titulares dos direitos autorais, nem sempre com resultado.<br>' + 
	'A editora reserva os direitos para o caso de comprovada titularidade.</p>' + 
	'<p>Reproduçnao proibida. Art. 184 do Código Penal e Lei 9.610, de 19 de fevereiro de 1998.</p>' + 
	'<p>Todos os direitos reservados.</p>' + 
	'<p>EDITORA MODERNA' + 
	'<p>Rua Padre Adelino, 758 – Belenzinho<br>São Paulo, SP – Brasil – CEP 03303-904<br>www.moderna.com.br</p>' + 
	'<p>2015</p>' + 
	'<p>Produzido no Brasil</p>';

Textos.ajuda = '<p>Este OED foi pensado e produzido para que sirva de modelo de como programar os OEDs da editora Moderna.</p><p>Os exemplos citados nele correspondem aos padrões que o desenvolvedor deve seguir. Esses exemplos não servem de base para todos os OEDs, são apenas exemplos de HTML e Javascript para o desenvolvedor.</p><p>Será entregue um manual explicativo para deixar mais claro o que pode, ou não pode, ser modificado. Há também comentários nos arquivos que podem ser modificados para facilitar a produção de um novo OED.</p>';

