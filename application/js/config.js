﻿Main.titulo = 'QUAL É O MAIS PRÓXIMO?';

Main.tituloWithAudio = true;
// Main.tituloWithAudio = false;
Main.hasPreRun = false;

InterfaceController.mostrarGaleria     = false;
InterfaceController.mostrarBtnMenu     = true;
InterfaceController.mostrarBtnReinicar = true;
InterfaceController.mostrarBtnAjuda    = true;

InterfaceController.galeria = {};
InterfaceController.niveis = {};

var filesJSON = { "files": [ { "source": "system/css/img/sprite.png", "type": "IMAGE", "size": 22301  } ] }